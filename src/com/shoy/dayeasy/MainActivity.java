package com.shoy.dayeasy;

import android.os.Bundle;
import org.apache.cordova.Config;
import org.apache.cordova.CordovaActivity;

/**
 * Created by luoyong on 2014/7/16.
 */
public class MainActivity extends CordovaActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.init();
        super.loadUrl(Config.getStartUrl());
    }
}
